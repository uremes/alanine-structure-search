{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ce843015",
   "metadata": {},
   "source": [
    "# Alanine 2D\n",
    "\n",
    "This tutorial demonstrates how Bayesian optimisation (BO) can be used in molecule structure search. You will run an optimisation experiment with [BOSS](https://cest-group.gitlab.io/boss/) and learn to examine the optimisation results.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "We continue to work with the alanine molecule that was studied in the previous exercises. Here we will examine the molecule structures that arise when we rotate the two bonds marked on the picture below. The optimisation task is to find the rotations that minimise the molecule energy.\n",
    "\n",
    "<img src=\"../data/alanine_2D.png\" alt=\"alanine\" width=\"450\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3bec451",
   "metadata": {},
   "source": [
    "## Instructions\n",
    "\n",
    "This notebook includes code cells that contain python code. We will run the cells to execute the code.\n",
    "\n",
    "\n",
    "To run a code cell, click on the cell to select it and then either click the run button <kbd>&#9658</kbd> that you find in the toolbar on top or press <kbd>Ctrl</kbd> + <kbd>Enter</kbd> to run the cell.\n",
    "\n",
    "Run this code cell to print 'hello!':"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd591727",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('hello!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4731e85f",
   "metadata": {},
   "source": [
    "To complete this tutorial, run all the code cells and examine the results."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6198994",
   "metadata": {},
   "source": [
    "## Imports\n",
    "\n",
    "Let us load external python code that we want to use in this notebook.\n",
    "\n",
    "- `BOMain` will be used to run optimisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acc88658",
   "metadata": {},
   "outputs": [],
   "source": [
    "from boss.bo.bo_main import BOMain"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e76cd0ed",
   "metadata": {},
   "source": [
    "- `examples` and `visualisation` contain codes that determine the optimisation task and visualise optimisation results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8191c1b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "from pathlib import Path\n",
    "code_path = Path.cwd().parent / \"code\"\n",
    "sys.path.append(str(code_path))\n",
    "import examples, visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b3ea1f1",
   "metadata": {},
   "source": [
    "## Task\n",
    "\n",
    "Load the optimisation task from `examples`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac6ae4e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "task = examples.Alanine(dim=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0c904f1",
   "metadata": {},
   "source": [
    "The example task comes with bounds that indicate the minimum and maximum value for each input variable. We can see that both input variables in the 2-dimensional alanine example take values between 0 and 360:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d6b1296",
   "metadata": {},
   "outputs": [],
   "source": [
    "task.bounds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78dffe2b-8c95-458a-88e5-a84efffeba26",
   "metadata": {},
   "source": [
    "Let us examine the molecule structure calculated based on selected input values. The alanine molecule should appear as an interactive 3D model below the code cell. Rotate the molecule with the mouse so that you can see the structure well.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21c8d5e8-c3ac-4bc4-8a68-bd87be9f5fcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "angle_1 = 0\n",
    "angle_2 = 0\n",
    "task.view((angle_1, angle_2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b991d44",
   "metadata": {},
   "source": [
    "The example task also includes an energy calculation method that takes the angles as input and returns the corresponding alanine molecule energy as output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dbe1b7e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "angle_1 = 0\n",
    "angle_2 = 0\n",
    "print('Energy: {:.2f} kcal/mol'.format(float(task.calculate_energy((angle_1, angle_2)))))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eefad742",
   "metadata": {},
   "source": [
    "Now our task is to find the inputs `angle_1` and `angle_2` that minimise the output from `task.calculate_energy`!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b43347",
   "metadata": {},
   "source": [
    "### Run optimisation\n",
    "\n",
    "Bayesian optimisation (BO) is an iterative optimisation method that uses a statistical model to capture what it knows about the unknown function that we want to minimise. Here we will initialise the model with `initpts` = 5 data points and run optimisation to collect `iterpts` = 40 data points. \n",
    "\n",
    "We record the selected initialisation and iteration counts in the optimisation setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71c24666",
   "metadata": {},
   "outputs": [],
   "source": [
    "setup = {'initpts': 5, 'iterpts': 40, 'kernel': 'stdp', 'yrange': [0, 20], 'seed': 231012}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acaa15c2",
   "metadata": {},
   "source": [
    "We prepare a `BOMain` optimisation run based on the optimisation task and optimisation setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ef27f7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(task.calculate_energy, task.bounds, **setup)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e344e19a",
   "metadata": {},
   "source": [
    "Now we can run optimisation. This will take around 1 minute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8de8a6fa",
   "metadata": {},
   "outputs": [],
   "source": [
    "results = bo.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "858cf165",
   "metadata": {},
   "source": [
    "### Examine results\n",
    "\n",
    "When the optimisation run is finished, the optimisation outcome is saved in the variable `results`. We can examine `results` to see what the optimisation run learned.\n",
    "\n",
    "##### 1.\n",
    "\n",
    "The optimisation run observed `initpts` + `iterpts` = 45 data points. The data points are called acquisitions. Each data point or acquisition includes the selected inputs and the observed output.\n",
    "\n",
    "Let us examine `results` to find the lowest observed energy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3924ec4",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Lowest energy: {:.2f} kcal/mol'.format(float(min(results.select('Y')))))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4675e1c6",
   "metadata": {},
   "source": [
    "##### 2.\n",
    "\n",
    "The optimisation method used in this tutorial creates a statistical model that can predict the molecule energy based on the input variables. The model predictions can be visualised as an energy map that uses colour to encode the predicted output values across the input space.\n",
    "\n",
    "Let us examine how the model evolved from iteration 0 to iteration 40 as optimisation acquired more data. We will create an animation that shows the observed input points and model predictions on the left and the observed output values on the right. Each frame in the animation corresponds to one iteration. This will take around 1 minute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0290c2e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualisation.animate_2d(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aae66e4f",
   "metadata": {},
   "source": [
    "How many iterations were needed to find the lowest observed value? Do you think optimisation found the lowest possible value?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea5fc6ca",
   "metadata": {},
   "source": [
    "##### 3.\n",
    "\n",
    "Examine the energy map at iteration 40."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87ee8301",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualisation.plot_2d(results, iteration=40)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d84b328e",
   "metadata": {},
   "source": [
    "What is the lowest point found on the map? Does it match the lowest energy found in `results`?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73b616ea",
   "metadata": {},
   "source": [
    "##### 4.\n",
    "\n",
    "Each data point corresponds to a molecule structure. We can visualise the molecule structures that were explored in the above optimisation run as an interactive animation where each frame corresponds to one iteration and one molecule structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9dc4fc7-f2bd-4a8b-a4aa-0937eca87a2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "task.view_all(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cac2fd1a",
   "metadata": {},
   "source": [
    "You should see the alanine molecule as an interactive 3D model. Rotate the molecule with the mouse so that you can see the structure well. A control bar appears on the lower left corner when you move your mouse over the molecule. The control bar includes play and stop buttons and a slider that shows the iteration number. You can use the slider to move between iterations.\n",
    "\n",
    "Can you find the structure with the lowest energy? How does the low energy structure differ from higher energy structures?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4fd0105",
   "metadata": {},
   "source": [
    "## What next\n",
    "\n",
    "That concludes the alanine 2D structure search tutorial. Well done! You can continue to the alanine 4D notebook or read more about\n",
    "\n",
    "- [alanine](https://en.wikipedia.org/wiki/Alanine) (Finnish: [alaniini](https://fi.wikipedia.org/wiki/Alaniini))\n",
    "- [Bayesian optimisation](https://en.wikipedia.org/wiki/Bayesian_optimization) (Finnish: [bayesilainen optimointi](https://fi.wikipedia.org/wiki/Bayesilainen_optimointi))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python (boss-tutorial-environment)",
   "language": "python",
   "name": "boss-tutorial-environment"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
