{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ce843015",
   "metadata": {},
   "source": [
    "# Alanine 4D\n",
    "\n",
    "This notebook extends the alanine 2D structure search tutorial. The optimisation experiment in the 2D tutorial considered rotations around two bonds in the alanine molecule structure. The optimisation experiment in this notebook examines rotations around four bonds."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6198994",
   "metadata": {},
   "source": [
    "## Imports\n",
    "\n",
    "- `BOMain` will be used to run optimisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acc88658",
   "metadata": {},
   "outputs": [],
   "source": [
    "from boss.bo.bo_main import BOMain"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e76cd0ed",
   "metadata": {},
   "source": [
    "- `examples` and `visualisation` contain codes that determine the optimisation task and visualise optimisation results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd65bfc5-829a-4b2c-bf94-fdee8710a1ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "from pathlib import Path\n",
    "code_path = Path.cwd().parent / \"code\"\n",
    "sys.path.append(str(code_path))\n",
    "import examples, visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b3ea1f1",
   "metadata": {},
   "source": [
    "## Task\n",
    "\n",
    "Load the optimisation task from `examples`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac6ae4e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "task = examples.Alanine(dim=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0c904f1",
   "metadata": {},
   "source": [
    "We can examine the task bounds to check that we now have four input variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d6b1296",
   "metadata": {},
   "outputs": [],
   "source": [
    "task.bounds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78dffe2b-8c95-458a-88e5-a84efffeba26",
   "metadata": {},
   "source": [
    "Here the second input variable takes values between 0 and 120. This is because the CH3 substructure is rotation symmetric with period 120° while the other substructures are rotation symmetric with period 360°."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eefad742",
   "metadata": {},
   "source": [
    "The optimisation task is to find the inputs that minimise the output from `task.calculate_energy`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b43347",
   "metadata": {},
   "source": [
    "### Run optimisation\n",
    "\n",
    "We use Bayesian optimisation (BO) to solve the optimisation task. Here we start with `initpts` = 5 data points and run optimisation to collect `iterpts` = 100 data points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71c24666",
   "metadata": {},
   "outputs": [],
   "source": [
    "setup = {'initpts': 5, 'iterpts': 100, 'kernel': 'stdp', 'yrange': [0, 20], 'seed': 231012, 'minfreq': 500}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acaa15c2",
   "metadata": {},
   "source": [
    "Prepare a `BOMain` optimisation run based on the optimisation task and optimisation setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ef27f7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(task.calculate_energy, task.bounds, **setup)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e344e19a",
   "metadata": {},
   "source": [
    "The optimisation run will take around 5 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8de8a6fa",
   "metadata": {},
   "outputs": [],
   "source": [
    "results = bo.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "858cf165",
   "metadata": {},
   "source": [
    "### Examine results\n",
    "\n",
    "##### 1.\n",
    "\n",
    "Let us find the lowest energy observed in the current optimisation run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3924ec4",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Lowest energy: {:.2f} kcal/mol'.format(float(min(results.select('Y')))))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f10dc15-245b-4d02-9467-8156cc1fdd22",
   "metadata": {},
   "source": [
    "Compare the result to the lowest energy found in the 2D tutorial. Has 4D optimisation found a lower energy structure than 2D optimisation?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73b616ea",
   "metadata": {},
   "source": [
    "##### 2.\n",
    "\n",
    "The lowest energy observation is saved in the `results` as the best acquisition. Let us visualise the molecule structure with the lowest observed energy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "877a23fe-dd00-419c-b237-7cc77e6cee6f",
   "metadata": {},
   "outputs": [],
   "source": [
    "task.view(results.get_best_acq()[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4fd0105",
   "metadata": {},
   "source": [
    "## What next\n",
    "\n",
    "Read more about\n",
    "- [alanine](https://en.wikipedia.org/wiki/Alanine) (Finnish: [alaniini](https://fi.wikipedia.org/wiki/Alaniini))\n",
    "- [Bayesian optimisation](https://en.wikipedia.org/wiki/Bayesian_optimization) (Finnish: [bayesilainen optimointi](https://fi.wikipedia.org/wiki/Bayesilainen_optimointi))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python (boss-tutorial)",
   "language": "python",
   "name": "boss-tutorial"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
