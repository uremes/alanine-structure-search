import numpy as np
import matplotlib.pyplot as plt

from IPython.display import HTML
from matplotlib.animation import FuncAnimation
from mpl_toolkits.axes_grid1 import make_axes_locatable

resolution = 100
setup_iter = {'marker': 'x', 'color': 'k', 'alpha': 0.75, 'zorder': 10}
setup_xmin = {'marker': '*', 'color': 'm', 's': 100, 'zorder': 10}
animation_setup = {'ymin': 17, 'step': 4}
labels = {'y': 'Energy', 'x': ['Angle 1', 'Angle 2']}

def plot_acquisitions(res):

    Y = res.select('Y').squeeze()
    best = np.minimum.accumulate(Y).squeeze()
    initpts = res.settings['initpts']
    iterpts = res.settings['iterpts']
    yname = labels.get('y', 'y')

    plt.figure(figsize=(10, 5))
    plt.scatter([0] * initpts, Y[:initpts], **setup_iter)
    plt.scatter(np.arange(iterpts) + 1, Y[initpts:], label='acquisition', **setup_iter)
    plt.plot(np.arange(iterpts + 1), best[initpts - 1:])
    plt.xlabel('BO iteration')
    rotation = 90 if len(yname) > 1 else 0
    plt.ylabel(r'{}'.format(yname), rotation=rotation, labelpad=10)
    plt.xlim(0, iterpts)
    plt.legend(loc='upper center', frameon=False, bbox_to_anchor=(0.5, 1.1))


def plot_2d(res, iteration):

    if res.settings.dim != 2:
        raise ValueError("Results must have two input dimensions.")

    itr = iteration
    if itr < 0 or itr > res.settings['iterpts']:
        raise ValueError(f"Iteration {itr} not included in the results.")

    bounds = res.bounds
    xp1 = np.linspace(bounds[0][0], bounds[0][1], resolution)
    xp2 = np.linspace(bounds[1][0], bounds[1][1], resolution)
    x1, x2 = np.meshgrid(xp1, xp2)

    model = res.reconstruct_model(itr)
    yp = model.predict(np.column_stack((x1.ravel(), x2.ravel())))[0]
    yp = yp.reshape(len(xp1), len(xp2))

    yname = labels.get('y', 'y')
    xname = labels.get('x', ['$x_1$', '$x_2$'])

    plt.figure(figsize=(7, 5))
    levels = 200
    cset = plt.contourf(xp1, xp2, yp, levels=levels)
    cbar = plt.colorbar(cset)
    rotation = 90 if len(yname) > 1 else 0
    cbar.set_label(r'{}'.format(yname), rotation=rotation, labelpad=10)
    
    X = model.X
    plt.scatter(X[:, 0], X[:, 1], label='acquisition', **setup_iter)
    plt.xlabel(r'{}'.format(xname[0]))
    plt.ylabel(r'{}'.format(xname[1]))
    plt.xticks(np.linspace(bounds[0][0], bounds[0][1], 7))
    plt.yticks(np.linspace(bounds[1][0], bounds[1][1], 7))

    xmin = res.select('x_glmin', itr)
    plt.scatter(xmin[0], xmin[1], label='predicted minimum', **setup_xmin)
    plt.legend(frameon=False, loc='upper center', bbox_to_anchor=(0.5, 1.1), ncol=2)


def animate_2d(res):

    ymin = animation_setup['ymin']
    step = animation_setup['step']
    
    if res.settings.dim != 2:
        raise ValueError("Results must have two input dimensions.")

    # load setup and acquisitions
    initpts = res.settings['initpts']
    iterpts = res.settings['iterpts']
    bounds = res.bounds
    X = res.select('X')
    Y = res.select('Y').squeeze()
    best = np.minimum.accumulate(Y).squeeze()

    # set levels
    ymin = ymin or res.get_est_yrange(-1)[0]
    step = step or round((res.get_est_yrange(-1)[1] - ymin)/5, 2)
    yticks = np.linspace(ymin, ymin + 5 * step, 6)
    levels = np.linspace(ymin, ymin + 5 * step + step/2, 100)

    # initialise figure
    fig, ax = plt.subplots(1, 2, figsize=(12, 4), width_ratios=[1, 1.5])
    yname = labels.get('y', 'y')
    xname = labels.get('x', ['$x_1$', '$x_2$'])
    
    # initialise model predictions
    model_init = res.reconstruct_model(0)
    xp1 = np.linspace(bounds[0][0], bounds[0][1], resolution)
    xp2 = np.linspace(bounds[1][0], bounds[1][1], resolution)
    x1, x2 = np.meshgrid(xp1, xp2)
    yp = model_init.predict(np.column_stack((x1.ravel(), x2.ravel())))[0]
    yp = yp.reshape(len(xp1), len(xp2))
    cset = [ax[0].contourf(xp1, xp2, yp, levels=levels, extend='max')]
    ax[0].set_xticks(np.linspace(bounds[0][0], bounds[0][1], 7))
    ax[0].set_yticks(np.linspace(bounds[1][0], bounds[1][1], 7))
    ax[0].set_xlabel(r'{}'.format(xname[0]))
    ax[0].set_ylabel(r'{}'.format(xname[1]))
    divider = make_axes_locatable(ax[0])
    cax = divider.append_axes('right', size='5%', pad=0.2)
    fig.colorbar(cset[0], cax=cax, ticks=yticks)

    # initialise acquisition locations
    scat_iter = ax[0].scatter(X[:initpts, 0], X[:initpts, 1], **setup_iter)
    ax[0].set_xlim(bounds[0])
    ax[0].set_ylim(bounds[1])

    # initialise observed values
    ax[1].scatter([0] * initpts, Y[:initpts], **setup_iter)
    scat_y = ax[1].scatter([], [], label='acquisition', **setup_iter)
    line_y, = ax[1].plot(0, best[initpts])
    ax[1].set_xlabel('BO iteration')
    rotation = 90 if len(yname) > 1 else 0
    ax[1].set_ylabel(r'{}'.format(yname), rotation=rotation, labelpad=10)
    ax[1].set_xlim(0, iterpts)
    ax[1].set_ylim(levels[0], levels[-1] + 25 * step/100)
    ax[1].set_yticks(yticks)
    ax[1].legend(loc='upper center', frameon=False, bbox_to_anchor=(0.5, 1.1))

    fig.tight_layout()
    plt.close()

    def update(frame):

        # update model and acquisition locations
        model = res.reconstruct_model(frame)
        yp = model.predict(np.column_stack((x1.ravel(), x2.ravel())))[0]
        yp = yp.reshape(len(xp1), len(xp2))
        for tp in cset[0].collections:
            tp.remove()
        cset[0] = ax[0].contourf(xp1, xp2, yp, levels=levels, extend='max')
        scat_iter.set_offsets(X[:initpts + frame])

        # update observed values
        data = np.stack((np.arange(frame) + 1, Y[initpts:initpts + frame])).T
        scat_y.set_offsets(data)
        line_y.set_xdata(np.arange(frame + 1))
        line_y.set_ydata(best[initpts - 1:initpts + frame])
        return (cset[0], scat_iter, scat_y, line_y)

    ani = FuncAnimation(fig=fig, func=update, frames=iterpts + 1, interval=250, repeat=False)
    return HTML(ani.to_jshtml())
