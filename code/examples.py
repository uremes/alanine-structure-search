from io import StringIO
from contextlib import redirect_stdout

import numpy as np
import GPy
import ase.io
import ase.visualize
from ase.io.trajectory import Trajectory

import gcutil

viewer_setup = {'type': 'ball+stick', 'params': {'aspectRatio': 5, 'roughness': 1, 'radius': 0.05}}

class Alanine():

    def __init__(self, dim, modelpath='../data'):

        if dim not in [2, 4]:
            raise ValueError(f'Alanine example is not available in dimension {dim}.')

        self.modelpath = modelpath
        model ='{}/model_{}D_E0.npz'.format(modelpath, dim)
        self.model = load_model(model)
        self.viewer_setup = viewer_setup

    @property
    def dim(self):
        return self.model.input_dim

    @property
    def bounds(self):
        return [[0, self.model.std_periodic.period[d]] for d in range(self.dim)]

    def calculate_energy(self, x):
        return self.model.predict(np.atleast_2d(x))[0]

    def convert(self, x):
        # load base structure
        atomnames, rconnect, r, aconnect, a, dconnect, d = gcutil.readzmat('{}/alanine.gzmat'.format(self.modelpath))

        # update rotations
        if self.dim == 2:
            d[0] = x[0]  # d4
            d[1] = x[0] + 120  # d5
            d[9] = x[1]  # d13
        if self.dim == 4:
            d[0] = x[0]  # d4
            d[1] = x[0] + 120  # d5
            d[3] = x[1]  # d7
            d[4] = x[1] + 120  # d8
            d[5] = x[1] + 240  # d9
            d[7] = x[2]  # d11
            d[8] = x[2] + 180  # d12
            d[9] = x[3]  # d13

        # convert to atoms
        f = StringIO()
        with redirect_stdout(f):
            gcutil.write_xyz(atomnames, rconnect, r, aconnect, a, dconnect, d)
        atoms = ase.io.read(StringIO(f.getvalue()), format='xyz')
        return atoms

    def view(self, x, setup=None):
        setup = setup or self.viewer_setup
        atoms = self.convert(np.squeeze(x))
        viewer = ase.visualize.view(atoms, viewer='ngl')
        viewer.view.representations = [setup]
        return viewer.children[0]

    def view_all(self, res, setup=None):
        setup = setup or self.viewer_setup
        iterpts = res.settings['iterpts']
        writer = Trajectory('alanine.traj', mode="w")
        atoms = self.convert(np.zeros(self.dim))
        writer.write(atoms)
        for itr in range(iterpts):
            x = res.select('X', itr + 1)
            atoms = self.convert(np.squeeze(x))
            writer.write(atoms)

        reader = Trajectory('alanine.traj', mode="r")
        viewer = ase.visualize.view(reader, viewer='ngl')
        viewer.view.representations = [setup]
        return viewer.children[0]


def load_model(filename):
    # load saved data
    saved = np.load(filename)
    dim = saved['X'].shape[1]

    # create kernel
    k = GPy.kern.StdPeriodic(input_dim=dim, ARD1=True, ARD2=True)

    # create mean function
    mf = GPy.mappings.Constant(dim, 1)

    # create model
    model = GPy.models.GPRegression(saved['X'], saved['Y'], kernel=k, mean_function=mf)

    # set model params
    model[:] = saved['params']
    model.fix()
    model.parameters_changed()

    return model
