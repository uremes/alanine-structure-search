# Alanine structure search

<a target="_blank" href="https://colab.research.google.com/drive/17MUT1oXkuRN7rIMOM3-ssMjdg_j-dNak?usp=sharing">
<img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/>
</a>

Run Bayesian optimisation to find the minimum energy structure for the alanine molecule. 

Tutorial notebooks prepared for Shaking up Tech 2023. Based on a previous tutorial by Milica Todorović: https://github.com/milicasan/shaking-tech
